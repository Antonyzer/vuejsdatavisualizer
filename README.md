# vuejsdatavisualizer

## Steps done

- Consult a paginated table of data.
- View the data in the form of graphs (pie chart & histogram).
- Search into the data table.
- Filter the data using a simple form.
- Consult the detail of each row of the table, and modify the data.
- Export the modified data as JSON.
- BONUS: Filter the data by clicking on the graphs.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
